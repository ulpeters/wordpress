### Wordpress

### Create a new site
1. Prepare a DB, see db/README.md 
2. Copy a new site from the template
3. Update `.env`
  - Update the vhost-name
  - Update the letsencrypt address
  - Remove example DB config
  - Copy the DB settings
4. Start the container `cd sites/<new site>` and `docker-compose up -d`

#### Example
```
cd /opt/docker/db/utils/
create_db.sh wwwexamplecom
cd /opt/docker/wordpress
mkdir -p sites
cp -R TEMPLATES/ sites/www.example.com
cd sites/www.example.com
sed -e 's/<fqdn>/www.example.com/' \
    -e 's/<site\.tld>/example\.com/' \
    -e '/^DB.*/d' \
    .env
echo "" >> .env #newline
cat /opt/docker/db/utils/wwwexamplecom__db.conf >> sites/<new site>/.env
```

#### Files to backup 
- /opt/docker/wordpress
- /opt/db_sqldump
