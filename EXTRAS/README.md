# EXTRAS
## LDAP
##### Enable
To enable the [LDAP php extension](https://www.php.net/manual/en/intro.ldap.php)
run `docker compose build` with [Dockerfile](Dockerfile) in ./build/ add following extra line in docker-compose.yml:

```
services:
  wordpress:
    build: ./build/
```

##### Verify
To list installed php externsions, run: 
```
docker exec -it <container_name> sh -c 'php -r "print_r(get_loaded_extensions());"'
```

##### Debug
`ldapsearch` from the Debian package [ldap-utils](https://packages.debian.org/search?keywords=ldap-utils) can be used to run queries for testing, e.g.:
```
ldapsearch -x -H 'ldap://ldap.example.com' -b 'cn=users,dc=example,dc=com' 'cn=johndoe'
```



## Custom php configuration
To add you custom php configuration, put [config.ini](config.ini) in ./conf/ and
add following extra line in `docker-compose.yml`:
```
services:
  wordpress:
    volumes:
      - ./conf/custom.ini:/usr/local/etc/php/conf.d/custom.ini:ro

```